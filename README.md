Статистика

1) Установка:

    virtualenv venv -p /usr/bin/python3.8
    
    source venv/bin/activate
    
    pip3 install -r requirements.txt
    
2) Настройки

    input_results = 'input_results'  # Файлы спарщенные с телеграмма

    convert_to_json_folder = 'convert_to_json'  # Файлы подготовленные для загрузки в нейронку

    ner_output_files = 'output_files'  # Файлы отработанные в нейронке

    bert_path = 'http://127.0.0.1:8017/bert_api/entities/'

3) Использование
    
    
    1) Подготовка файлов полученных из парсера телеграмма для загрузки в нейронку
        Загружаем файлы в input_files
        python3 converter.py
        Файлы перейдут в convert_to_json в формате ["text1", "text2"]
    2) Отправка файлов в нейронку
        python3 create_output_files.py
        Файлы отправляются по одному в нейронку после получения ответа отправляются в output_files
        Файлы перейдут в output_files в формате: 
        {"data": [{"text": "text1", "entities": ["entity1", "entity2"]}, ...]}
        
    3) Получение статистики
        --filename входной файл(например: output_files/Sputnik_results.txt)
        --output_file выходной файл
        Для получения всей статистики
        python3 all_stats.py --filename <name> --output_file <output_file>
        
        Для получения статистики по сущностям
        --output_file (например: output_files/Sputnik_results.txt)
        python3 entity_stats.py --filename <name> --output_file <output_file>
    
4) Результаты
    результаты лежат в all_stat.csv и в entity_stat.csv

5) Краткий порядка выполнения
    
    
    1) Запустить докер с нейронкой
    2) Запустить парсер и скачать сообщения групп, закинуть в папку input_results
    3) python3 converter.py
    4) python3 create_output_files.py (отправит в нейронку, докер должен быть запущен)
    5) python3 all_stats.py --filename <name> --output_file <output_file>
    6) python3 entity_stats.py --filename <name> --output_file <output_file>
    7) Результаты лежат в all_stat.csv и в entity_stat.csv