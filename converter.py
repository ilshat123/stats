import argparse
import json
import os
from pprint import pprint

import requests

import config


def convert_all_input_files_to_json():
    files = os.listdir(config.input_results)
    for file in files:
        with open(os.path.join(config.input_results, file), 'r', encoding='utf-8') as f:
            texts = []
            for elem in (elem for elem in f.read().split('\n') if elem != ''):
                elem = json.loads(elem)
                if elem['content']['@type'] == 'messageText':
                    text = elem['content']['text']['text']
                    # print(text)
                    texts.append(text)

        with open(os.path.join(config.convert_to_json_folder, file), 'w', encoding='utf-8') as f:
            f.write(json.dumps(texts, ensure_ascii=False))


def converter_with_split_on_files(elem_count_in_file):
    files = os.listdir(config.input_results)
    for file in files:
        with open(os.path.join(config.input_results, file), 'r', encoding='utf-8') as f:
            texts = []
            n = 0
            for elem in (elem for elem in f.read().split('\n') if elem != ''):
                elem = json.loads(elem)
                if elem['content']['@type'] == 'messageText':
                    text = elem['content']['text']['text']
                    # print(text)
                    texts.append(text)

                if len(texts) > elem_count_in_file:
                    with open(os.path.join(config.convert_to_json_folder, f'{file}__{n}'), 'w', encoding='utf-8') as f:
                        f.write(json.dumps(texts, ensure_ascii=False))
                    texts = []
                    n += 1

        with open(os.path.join(config.convert_to_json_folder, f'{file}__{n}'), 'w', encoding='utf-8') as f:
            f.write(json.dumps(texts, ensure_ascii=False))


if __name__ == '__main__':
    convert_all_input_files_to_json()
    # converter_with_split_on_files(1000)
