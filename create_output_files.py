import json
import os

import requests

import config


def create_output_files():
    files = os.listdir(config.convert_to_json_folder)
    for file in files:
        with open(os.path.join(config.convert_to_json_folder, file), 'rb') as f:
            text = str(f.read(), encoding='utf-8')
            js = {'texts': json.loads(text)}
            r = requests.post(config.bert_path, data=json.dumps(js))
            texts = json.loads(r.text)

        with open(os.path.join(config.ner_output_files, file), 'w', encoding='utf-8') as f:
            f.write(json.dumps(texts, ensure_ascii=False))


if __name__ == '__main__':
    create_output_files()
