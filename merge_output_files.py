import json
import os

import config


def merge_output():
    files = os.listdir(config.ner_output_files)
    files = [elem.split('__') for elem in files if '__' in elem]

    group_groups = {}
    for file in files:
        name, num = file
        print(name)
        print(group_groups.get(name))
        ll = group_groups.get(name)
        if ll:
            ll.append(f'{name}__{num}')
        else:
            ll = []
            ll.append(f'{name}__{num}')
        group_groups[name] = ll

    for group, elems in group_groups.items():
        merge_list = []
        for elem in elems:
            print(elem)
            with open(os.path.join(config.ner_output_files, elem), 'r', encoding='utf-8') as file:
                merge_list.extend(json.loads(file.read())['data'])

        with open(os.path.join(config.merge_files, group), 'w', encoding='utf-8') as file:
            file.write(json.dumps({'data': merge_list}, ensure_ascii=False))

merge_output()