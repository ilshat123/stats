import argparse
import json
import os
import re
import string
import csv

import pymorphy2

import config

morph = pymorphy2.MorphAnalyzer()
stat_dict = {}


def get_stat(filename, output_file):
    """статистика по всему тексту"""
    with open(filename, encoding='utf-8') as file:
        texts = json.loads(file.read())
        texts = (elem['text'] for elem in texts['data'])
        for text in texts:
            words = re.findall(r'[\w\d]+', text)
            for word in words:
                word = morph.parse(word)[0].normal_form
                stat_dict[word] = stat_dict.get(word, 0) + 1

        for key in list(stat_dict):
            if key in string.punctuation:
                del stat_dict[key]

        f = open(output_file, 'w+', encoding='utf-8')
        with f:
            writer = csv.writer(f)
            for key, num in sorted(stat_dict.items(), key=lambda x: -x[1]):
                writer.writerow([key, num])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--filename", type=str)
    parser.add_argument("--output_file", type=str, default='all_stat.csv')
    args = parser.parse_args()
    get_stat(args.filename, args.output_file)
    # stat_file = 'merge_files/sputnik_is_starting.txt'
    # get_stat(stat_file, 'all_stat.csv')
