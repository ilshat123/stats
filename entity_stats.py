import argparse
import json
import re
import string
import csv

import pymorphy2

morph = pymorphy2.MorphAnalyzer()


def get_stat(filename, output_file):
    """статистика по сущностям"""
    stat_dict = {}
    f = open(filename, encoding='utf-8')
    data = json.load(f)
    data = data['data']
    for ex in data:
        ent_list = ex['entities']
        for ent in ent_list:
            text = re.findall(r'[\w\d]+', ent['text'])
            s = " ".join(morph.parse(word)[0].normal_form for word in text)
            stat_dict[s] = stat_dict.get(s, 0) + 1

    f = open(output_file, 'w+', encoding='utf-8')
    with f:
        writer = csv.writer(f)
        for key, num in sorted(stat_dict.items(), key=lambda x: -x[1]):
            writer.writerow([key, num])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--filename", type=str)
    parser.add_argument("--output_file", type=str, default='entity_stat.csv')
    args = parser.parse_args()
    get_stat(args.filename, args.output_file)
    # get_stat('merge_files/sputnik_is_starting.txt', 'ent_stat.csv')
